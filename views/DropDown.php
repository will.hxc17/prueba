<!-- Apartado para mostrar foto y nombre del usuario logueado -->
<div class="top-nav notification-row">

    <ul class="nav pull-right top-menu">
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <span class="profile-ava">
                <!-- Cargamos la imagen y nombre -->
            <img src="<?php echo $urlViews; ?>img/user.png" alt="Usuario" height="35" width="35">
            </span>
            <span class="username"><?php echo $UserLogueado; ?></span>
            <b class="caret"></b>            
            </a>
            <?php include ("MenuOpcion.php") ?>
        </li>
    </ul>

</div>