<!-- archivo para incluir estilos y herramientas a la cabecera de la pag -->
<head>
    <meta http-equiv="Content-Type" content="" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema para el Menejo de Empresas - contabilidad , ventas , ingresos , egresos">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Sistema modo tactil , para escritorio tabletas ">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>XXXXX - Sistema Profesional Para Estudiantes</title>

    <!-- $urlViews variable que se creo en el archivo constans para sarcar de manera mas facil
        las rutas de los estilos y herramientas
    -->

    <!-- Bootstrap CSS -->
    <link href="<?php echo $urlViews; ?>css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?php echo $urlViews; ?>css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="<?php echo $urlViews; ?>css/elegant-icons-style.css" rel="stylesheet">
    <link href="<?php echo $urlViews; ?>css/font-awesome.min.css" rel="stylesheet">
    <!-- full calendar css-->
    <link href="<?php echo $urlViews; ?>css/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet">
    <link href="<?php echo $urlViews; ?>css/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet">
      <!-- owl carousel -->
    <link rel="stylesheet" href="<?php echo $urlViews; ?>css/owl.carousel.css" type="text/css">
    <link href="<?php echo $urlViews; ?>css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <link rel="stylesheet" href="<?php echo $urlViews; ?>css/fullcalendar.css">
    <link href="<?php echo $urlViews; ?>css/widgets.css" rel="stylesheet">
    <link href="<?php echo $urlViews; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo $urlViews; ?>css/style_pru.css" rel="stylesheet">
    <link href="<?php echo $urlViews; ?>css/style-responsive.css" rel="stylesheet">
    <link href="<?php echo $urlViews; ?>css/xcharts.min.css" rel=" stylesheet">
    <link href="<?php echo $urlViews; ?>css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <link href="<?php echo $urlViews; ?>css/anchomodal.css" rel="stylesheet">

    <!-- Subir Imagen -->
    <link href="<?php echo $urlViews; ?>css/fileinput.css" media="all" rel="stylesheet" type="text/css">
    <!-- Calendario -->
    <link href="<?php echo $urlViews; ?>css/calendario.css" rel="stylesheet">

    <link href="<?php echo $urlViews; ?>js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Ventana modal -->
    <link rel="stylesheet" href="<?php echo $urlViews; ?>css/basicModal.min.css">

    <!-- Ventana modal style -->
    <link rel="stylesheet" type="text/css" href="<?php echo $urlViews; ?>css/component.css">

    <link rel="stylesheet" href="<?php echo $urlViews; ?>css/ngDialog.css">
    <link rel="stylesheet" href="<?php echo $urlViews; ?>css/ngDialog-theme-default.css">
    <link rel="stylesheet" href="<?php echo $urlViews; ?>css/ngDialog-theme-plain.css">
    <link rel="stylesheet" href="<?php echo $urlViews; ?>css/ngDialog-custom-width.css">


    <link href='<?php echo $urlViews; ?>css/adaptive-modal.css' rel='stylesheet' type='text/css'>



</head>