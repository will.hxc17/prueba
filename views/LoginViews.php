<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
<!-- Titulo pagina y links bootstrap y estilos de pagina -->
    <title>(XXXXX) - Sistema Profesional Para Estudiantes</title>
    <!-- Bootstrap CSS -->
    <link href="views/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="views/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="views/css/elegant-icons-style.css" rel="stylesheet">
    <link href="views/css/font-awesome.css" rel="stylesheet">
    <!-- Custom styles -->
    <link href="views/css/style.css" rel="stylesheet">
    <link href="views/css/style-responsive.css" rel="stylesheet">

</head>
<!-- Comienzo Cuerpo de inicio de sesion y caja para inicio de sesion -->
<body class="login-img3-body">

<div class="container">

    <form class="login-form" action="./controller/AccessUser.php">
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <div class="input-group">
                <span class="input-group-addon"><i class=""></i></span> <!-- Icono para la caja -->
                <input type="text" name="usuario"   class="form-control" placeholder="Nombre de Usuario" autofocus>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class=""></i></span> <!-- Icono para la caja -->
                <input type="password"  name="password" class="form-control" placeholder="Contraseña">
            </div>
            <!-- Control para recordar usuario u olvidar contraseña -->
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Recordar
                <span class="pull-right"> <a href="#"> Olvidaste tu contraseña?</a></span>
            </label>
            <!-- Botones para acceder o registrarse -->
            <button class="btn btn-primary btn-lg btn-block" type="submit">Iniciar Sesion</button>
            <button class="btn btn-danger btn-lg btn-block" type="submit">Registrarme</button>
        </div>
    </form>
</div>
</body>
</html>