<?php
/* <!-- Variables para la conexion mysql --> */
class conexion {
    private $user;
    private $password;
    private $server;
    private $database;
    private $con;
/* Conexion a la base de datos mysql */
    public function __construct(){
        $user = 'root';
        $password = '';
        $server = 'localhost';
        $database = 'pruebapos';
        $this->con = new mysqli($server,$user,$password,$database);
        
    }
/* Funcion para validar datos en base */
    public function getUser($usuario , $password){
        /* Consulta para validar usuario y contraseña */
        $query = $this->con->query("SELECT * FROM usuarios WHERE login='" . $usuario . "' AND password='" . $password . "'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;

    }

        /* Funcion menu principal admin */
    public function getMenuMain(){
        /* Consulta para validar tabla menu */
        $query = $this->con->query("SELECT * FROM `menu`");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;

    }


        /* Funcion para acceso del usuario ventas */
    public function getMenuMainVentas(){
        /* Consulta para validar tabla menu */
        $query = $this->con->query("SELECT * FROM `menu` WHERE acceso='A'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;

    }

/*=============================================
=              SECCION USUARIO                =
=============================================*/

            /* Funcion para mostrar tabla */
    public function getAllUserData(){
        /* Consulta para cargar tabla de usuarios */
        $query = $this->con->query("SELECT * FROM usuarios ");

        return $query;

    }

                        /* Funcion para crear usuarios nuevos */
    public function getRegisterNewUser($nombre,$tipo,$usuario,$password){
        /* Consulta para cargar tabla de usuarios */
        $query = $this->con->query("INSERT INTO `usuarios`(`id_usu`, `login`, `tipo`, `nombre`, `password`)
                                    VALUES (NULL,'$usuario','$tipo','$nombre','$password')");

        return $query;

    }

            /* Funcion para eliminar usuarios */
    public function DeleteUsuario($idUsuario){

        $query = $this->con->query("DELETE FROM usuarios WHERE id_usu=$idUsuario");

        return $query;
    }
/* Funcion para tomar los datos y actualizarlos */
    public function UpdateUsuario($usuario,$tipo,$nombre,$password,$idUsuario){

        $query = $this->con->query("UPDATE `usuarios`
        SET `login`='$usuario',
            `tipo`='$tipo',
            `nombre`='$nombre',
            `password`='$password' WHERE `usuarios` . `id_usu`='$idUsuario'");

        return $query;
    }


/*=============================================
=             SECCION MENSAJES                =
=============================================*/


/* Funcion recorrido tabla alerta en base */
    public function getMensajeAlerta(){

        $query = $this->con->query("SELECT * FROM alerta ");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;

    }
    /* Funcion para actualizar el mensaje en la tabla alerta */
    public function UpdateMensajeAlerta($mensaje,$alerta)
    {
        $query = $this->con->query("UPDATE `alerta` 
        SET `tipoAlerta`='$alerta',`mensaje`='$mensaje' WHERE `alerta`.`alertaId`=1");

        return $query;
    }

/*=============================================
=               SECCION FACTURA               =
=============================================*/

    /* Funcion para cargar propietario */
public function getDataFactura(){
    $query = $this->con->query("SELECT * FROM `datos`");

    return $query;
}
/* Funcion para actualizar datos propietario en factura */
public function UpdateDataFactura($propietario,$razon,$direccion,$nro,$telefono){
    $query = $this->con->query("UPDATE `datos` 
                        SET `propietario`='$propietario',
                        `razon`='$razon',
                        `direccion`='$direccion',
                        `nro`='$nro',
                        `telefono`='$telefono' WHERE `datos`.`iddatos`=1");

        return $query;
}

/*=============================================
=           SECCION PROVEEDOR                 =
=============================================*/


public function getAllProveedor(){
    /* Consulta para cargar tabla de proveedor */
    $query = $this->con->query("SELECT * FROM proveedor ");

    return $query;

}

/* Funcion provedor nuevo */
public function registerNewProveedor($proveedor,$responsable,$fechaRegistro,$direccion,$telefono)
{

    $query = $this->con->query("INSERT INTO `proveedor` (`idproveedor`, `proveedor`, `responsable`, `fechaRegistro`, `direccion`, `telefono`)
                                VALUES (NULL, '$proveedor', '$responsable', '$fechaRegistro', '$direccion', '$telefono') ");

    return $query;
}

/* Funcion eliminar proveedor */
public function deleteProveedor($idProveedor)
{
    $query = $this->con->query("DELETE FROM proveedor WHERE idproveedor=$idProveedor ");

    return $query;
}

/* Funcion actualizar provedor */
public function updateProveedor($idProveedor, $proveedor, $responsable, $direccion, $telefono, $fechaRegistro)
{

    $query = $this->con->query("UPDATE `proveedor` SET `proveedor` = '$proveedor', 
                                        `responsable` = '$responsable', 
                                        `fechaRegistro` = '$fechaRegistro', 
                                        `direccion` = '$direccion',
                                         `telefono` = '$telefono' WHERE `proveedor`.`idproveedor` = $idProveedor");

    return $query;
}

/*=============================================
=              SECCION CLIENTE                =
=============================================*/


public function getAllCliente()
{

    $query = $this->con->query("SELECT * FROM cliente ");

    return $query;
}
/* Registro cliente */
public function registerNewCliente($imagen, $nombre, $apellido, $direccion, $telefonoFijo, $telefonoCelular, $email, $ci)
{

    $query = $this->con->query("INSERT INTO `cliente` (`idcliente`, `foto`, `nombre`, `apellido`, `direccion`, `telefonoFijo`, `telefonoCelular`, `email`, `contactoReferencia`, `telefonoReferencia`, `observaciones`, `ci`) 
                                 VALUES (NULL, '$imagen', '$nombre', '$apellido', '$direccion', '$telefonoFijo', '$telefonoCelular', '$email', '', '', '', '$ci')");

    return $query;
}

/* Actualizar cliente */
public function updateClient($idcliente, $imagen, $nombre, $apellido, $direccion, $telefonoFijo, $telefonoCelular, $email, $ci)
{

    $query = $this->con->query("UPDATE `cliente` SET 
                                            `foto` = '$imagen', 
                                            `nombre` = '$nombre', 
                                            `apellido` = '$apellido', 
                                            `direccion` = '$direccion',
                                            `telefonoFijo` = '$telefonoFijo', 
                                            `telefonoCelular` = '$telefonoCelular', 
                                            `email` = '$email', 
                                            `ci` = '$ci' WHERE `cliente`.`idcliente` = $idcliente");

    return $query;
}
/* Eliminar cliente */
public function deleteClient($idClient)
{
    $query = $this->con->query("DELETE FROM cliente WHERE idcliente=$idClient ");

    return $query;
}



/*=============================================
=                 SECCION PRODUCTO            =
=============================================*/

/* Tipo producto
-------------------------------------------------- */

public function getAllTipoProducto()
{
    $query = $this->con->query("SELECT * FROM tipoproducto");

    return $query;
}
/* Registrar tipoproducto */
public function registerNewTipoProduct($tipoProducto)
{
    $query = $this->con->query("INSERT INTO `tipoproducto` (`idtipoproducto`, `tipoproducto`) 
                                      VALUES (NULL, '$tipoProducto')");

    return $query;

}
/* Funcion eliminar tipoproducto */
public function deleteTipoProduct($tipoProductoId)
{
    $query = $this->con->query("DELETE FROM tipoproducto WHERE idtipoproducto=$tipoProductoId");

    return $query;
}
/* Funcion actualizar tipoproducto */
public function updateTipoProducto($tipoProductoId, $tipoproducto)
{
    $query = $this->con->query("UPDATE `tipoproducto` SET `tipoproducto` = '$tipoproducto'
                                      WHERE `tipoproducto`.`idtipoproducto` = $tipoProductoId");

    return $query;
}


/* Producto
---------------------------------- */

public function getProductoElegido($idproducto){
    
        $query = $this->con->query("SELECT * FROM `producto` WHERE idproducto='$idproducto'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;

    }

/* Preventa producto */
    public function insertarPreventaProducto($imagen, $producto, $precio, $idProducto, $pventa, $idUser, $tipo){

        $query = $this->con->query("INSERT INTO `preventa` (`idPreventa`, `imagen`, `producto`, `precio`, `idProducto`, `pventa`, `idUser`, `tipo`)
                                          VALUES (NULL, '$imagen', '$producto', '$precio', '$idProducto', '$pventa', '$idUser', '$tipo')");

        return $query;
    }







/* Productos */
    public function getAllProducto(){


        $query = $this->con->query("SELECT * FROM producto");

        return $query;
    }

/* Producto nuevo */
    public function registerNewProducto($imagen, $codigo, $nombreProducto, $cantidad, $precioVenta, $tipo, $proveedor, $precioCompra){
        
        $query = $this->con->query("INSERT INTO `producto` (`idproducto`, `imagen`, `codigo`, `nombreProducto`, `cantidad`, `precioVenta`, `tipo`, `proveedor`, `precioCompra`) 
                                          VALUES (NULL, '$imagen', '$codigo', '$nombreProducto', '$cantidad', '$precioVenta', '$tipo', '$proveedor', '$precioCompra')");

        return $query;
    }
/* Eliminar producto */
    public function deleteProduct($idproducto){

        $query = $this->con->query("DELETE FROM producto WHERE idproducto=$idproducto");

        return $query;
    }
/* Actualizar producto */
    public function updateProduct($imagen, $codigo, $nombreProducto, $cantidad, $precioVenta, $tipo, $proveedor, $precioCompra, $idproducto){

        $query = $this->con->query("UPDATE `producto` SET `imagen` = '$imagen', 
                                                     `codigo` = '$codigo',
                                                     `nombreProducto` = '$nombreProducto',
                                                     `cantidad` = '$cantidad', 
                                                     `precioVenta` = '$precioVenta', 
                                                     `tipo` = '$tipo',
                                                      `proveedor` = '$proveedor', 
                                                      `precioCompra` = '$precioCompra' WHERE `producto`.`idproducto` = $idproducto");

        return $query;
    }



/*=============================================
=               SECCION PEDIDO                =
=============================================*/

/* Mostrar pedido */
public function getAllPedido()
{
$query = $this->con->query('SELECT * FROM pedido order by idpedido desc ');
return $query;
}

/* Registrar pedido */

    public function registerNewPedido($descripcion, $total, $empresa, $usuario, $fechaRegistro)
{
$query = $this->con->query("INSERT INTO `pedido` (`idPedido`, `descripcion`, `total`, `proveedor`, `usuario`, `fechaRegistro`) 
                                VALUES (NULL, '$descripcion', '$total', '$empresa', '$usuario', '$fechaRegistro')");
return $query;
}

/* Eliminar pedido */

    public function deletePedido($idPedido)
{
$query = $this->con->query("DELETE FROM pedido WHERE idPedido=$idPedido");
return $query;
}

/* Actualizar pedido */

    public function updatePedido($descripcion, $total, $proveedor, $usuarioLogin, $fechaRegistro, $idPedido)
{
$query = $this->con->query("UPDATE `pedido` SET `descripcion` = '$descripcion',
                                        `total` = '$total', `proveedor` = '$proveedor',
                                         `usuario` = '$usuarioLogin', `fechaRegistro` = '$fechaRegistro'
                                          WHERE `pedido`.`idPedido` = $idPedido ");
return $query;
}


/*=============================================
=              SECCION PREVENTA               =
=============================================*/



public function getOnlyUserData($idUser)
{

    $query = $this->con->query("SELECT * FROM usuarios WHERE id_usu=$idUser");

$retorno = [];

$i = 0;
while ($fila = $query->fetch_assoc()) {
    $retorno[$i] = $fila;
    $i++;
}
return $retorno;

}



/* Vista Preventa */
    public function getPreventa()
    {
    $query = $this->con->query("SELECT idPreventa, imagen, producto, COUNT(producto)
     AS cantidad, SUM(precio) AS totalPrecio, idProducto, pventa, idUser, precio, tipo
      FROM `preventa` GROUP BY producto, idProducto, tipo ORDER BY idPreventa ASC ");
    return $query;
    }

/* Total preventa */
    public function getTotalPreventa()
{
$query = $this->con->query("SELECT SUM(precio) AS total, idUser FROM `preventa`");

return $query;
}


/* Limpiar preventa */
public function cleanRegistroPreventa()
{
$query = $this->con->query("TRUNCATE `preventa`");
return $query;
}
/* registro preventa */
public function registrarDatosPreventa($ci, $nombre, $totalAPagar, $efectivo, $cambio, $fechaVenta, $idcliente)
{
$query = $this->con->query("INSERT INTO `clientedato` (`idCliente`, `nombre`, `ci`, `fecha`, `totalApagar`, `efectivo`, `cambio`, `idClientei`, `tipoVenta`) 
                                VALUES (NULL , '$nombre', '$ci', '$fechaVenta', '$totalAPagar', '$efectivo', '$cambio', '$idcliente', 'Local');");
return $query;
}
/* eliminar producto de preventa */
public function deleteOnlyPreventa($idProducto, $tipo)
{
$query = $this->con->query("DELETE FROM preventa WHERE idproducto='$idProducto'  AND  tipo='$tipo'");
return $query;
}
/* eliminar toda la preventa */
public function deleteAllPreventa()
{
$query = $this->con->query("TRUNCATE `preventa`");
return $query;
}

/* Cambio productos */

    public function getDataProductoChoose($idProducto, $tipo)
{

$query = $this->con->query("SELECT * FROM `preventa` WHERE idproducto='$idProducto' AND tipo='$tipo'");

$retorno = [];

$i = 0;
while ($fila = $query->fetch_assoc()) {
    $retorno[$i] = $fila;
    $i++;
}
return $retorno;

}

/* Cantidad de productos */

    public function getCantidadProductoChoose($idProducto, $tipo)
{
$query = $this->con->query("SELECT count(idproducto) AS cantidadTotal FROM `preventa` WHERE idproducto='$idProducto' AND tipo='$tipo'");

$retorno = [];

$i = 0;
while ($fila = $query->fetch_assoc()) {
    $retorno[$i] = $fila;
    $i++;
}
return $retorno;

}



















}
?>
