<?php

/* Iniciamos conexion con base datos */
require_once ('../model/Conexion.php');
require ('./Constans.php');
if (!isset($_SESSION)){
    session_start();
}

/* Tomamos las variables id para la consulta */
$usuario = $_GET['usuario'];
$password = $_GET['password'];

/* verificamos variables en base de datos */
$con = new Conexion();
$searchUser = $con->getUser($usuario,$password);

/* sacamos los valores de la consulta y las volvemos variables */
foreach ($searchUser as $user){
    $idUsuario = $user['id_usu'];
    $tipo = $user['tipo'];
    $login = $user['login'];
    $nombre = $user['nombre'];
    $password = $user['password'];

}
/* mensaje de error si la busqueda es vacia */
if (empty($searchUser)){
    echo '
    <script language = javascript>
    alert("Usuario o contraseña incorrectas, por favor intente nuevamente")
    self.location ="../Index.php"
    </script>
    ';
}else if($tipo == 'VENTAS'){        /* tipo de sesion para la redirecion de pag. */
    $urlViews = URL_VIEWS;
    $UserLogueado = $nombre;
    $menuMain = $con->getMenuMainVentas(); /* Menu para el usuario de ventas */

    require ('../views/WelcomeVentas.php');
}



else if($tipo == 'ADMINISTRADOR'){      /* tipo de sesion para la redirecion de pag. */
    $urlViews = URL_VIEWS;
    $UserLogueado = $nombre;
    /* $ImagenUser = $foto; */

    $menuMain = $con->getMenuMain(); /* Menu para usuario administrador */
    
    require ('../views/Welcome.php');

}

?>