<?php
/*LLamado a Conexion con la base de datos */
require_once ('../model/Conexion.php');
require ('Constans.php');

if (!isset($_SESSION)) {
    session_start();
}

$con = new Conexion();

if (isset($_POST['new_proveedor'])) {
    $UsuarioLogin = $_POST['UsuarioLogin'];
    $PasswordLogin = $_POST['PasswordLogin'];
    $proveedor = $_POST['proveedor'];
    $responsable = $_POST['responsable'];
    $fechaRegistro = $_POST['fechaRegistro'];
    $direccion = $_POST['direccion'];
    $telefono = $_POST['telefono'];

    $mensaje = "Se agrego un Proveedor correctamente !!!";
    $alerta = "alert alert-success";

    $UpdateMensaje = $con->UpdateMensajeAlerta($mensaje, $alerta);

    $registrarNewProveedor = $con->registerNewProveedor($proveedor, $responsable, $fechaRegistro,$direccion, $telefono);


}


if (isset($_GET['idborrar'])) {
    $UsuarioLogin = $_GET['UsuarioLogin'];
    $PasswordLogin = $_GET['PasswordLogin'];
    $idProveedor = $_GET['idborrar'];

    $mensaje = "Se eliminaron los datos del Proveedor correctamente !!!";
    $alerta = "alert alert-danger";
    $UpdateMensaje = $con->UpdateMensajeAlerta($mensaje, $alerta);

    $deleteProveedor = $con->deleteProveedor($idProveedor);


}

if (isset($_POST['update_proveedor'])) {
    $idProveedor = $_POST['idproveedor'];
    $UsuarioLogin = $_POST['UsuarioLogin'];
    $PasswordLogin = $_POST['PasswordLogin'];
    $proveedor = $_POST['proveedor'];
    $responsable = $_POST['responsable'];
    $fechaRegistro = $_POST['fechaRegistro'];
    $direccion = $_POST['direccion'];
    $telefono = $_POST['telefono'];

    $mensaje = "Se Actualizaron  los datos del Proveedor correctamente !!!";
    $alerta = "alert alert-info";

    $UpdateMensaje = $con->UpdateMensajeAlerta($mensaje, $alerta);

    $updateProveedor = $con->updateProveedor($idProveedor, $proveedor, $responsable, $direccion, $telefono, $fechaRegistro);


}

$SearchUser = $con->getUser($UsuarioLogin, $PasswordLogin);
$AllUsuarios = $con->getAllUserData();

foreach ($SearchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
}


$MenuMain = $con->getMenuMain();
header("Location: Proveedor.php?usuario=$UsuarioLogin&password=$PasswordLogin&estado='Activo'");


?>