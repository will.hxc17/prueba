<?php 
require_once ('../model/Conexion.php');
require ('Constans.php');

if (!isset($_SESSION)){
    session_start();
}
$UsuarioLogin = $_POST['UsuarioLogin'];
$PasswordLogin = $_POST['PasswordLogin'];

$con = new Conexion();
$AllUsuarios = $con->getAllUserData();
$menuMain = $con->getMenuMain();

/* Funcion Agregar Usuario */
if (isset($_POST['nuevo_usuario'])){
$usuario = $_POST['login'];
$tipo = $_POST['tipo'];
$nombre = $_POST['nombre'];
$password = $_POST['password'];

$mensaje = "Se ha agregado un usuario nuevo al sistema";
$alerta = "alert alert-success";

$UpdateMensaje = $con->UpdateMensajeAlerta($mensaje, $alerta);

$RegisterNewUser = $con->getRegisterNewUser($nombre,$tipo,$usuario,$password);

}

/* Funcion eliminar Usuario */
if(isset($_GET['idborrar'])){
    $idUsuario = $_GET['idborrar'];
    $UsuarioLogin = $_GET['UsuarioLogin'];
    $PasswordLogin = $_GET['PasswordLogin'];

    $mensaje = "Se ha eliminado un usuario del sistema";
    $alerta = "alert alert-danger";

    $UpdateMensaje = $con->UpdateMensajeAlerta($mensaje, $alerta);

    $deleteUser = $con->DeleteUsuario($idUsuario);
}


/* Funcion Actualizar Usuario */
if(isset($_POST['update_usuario'])){
    $idUsuarioData = $_POST['idUsuario'];
    $usuario = $_POST['login'];
    $tipo = $_POST['tipo'];
    $nombre = $_POST['nombre'];
    $password = $_POST['password'];

    $UsuarioLogin = $_POST['UsuarioLogin'];
    $PasswordLogin = $_POST['PasswordLogin'];

    $mensaje = "Se Actualizaron los datos de un usuario";
    $alerta = "alert alert-info";

    $UpdateMensaje = $con->UpdateMensajeAlerta($mensaje, $alerta);

    $UpdateUser = $con->UpdateUsuario($usuario,$tipo,$nombre,$password,$idUsuarioData);
}
















header("Location: Usuario.php?usuario=$UsuarioLogin&password=$PasswordLogin&estado='Activo'");


?>