<?php
/* Conexion base datos */ 
require_once ('../model/Conexion.php');
require ('Constans.php');

if (!isset($_SESSION)){
    session_start();
}
$UsuarioLogin = $_POST['UsuarioLogin'];
$PasswordLogin = $_POST['PasswordLogin'];

$con = new Conexion();
$AllUsuarios = $con->getAllUserData();
$menuMain = $con->getMenuMain();

if(isset($_POST['update_data_factura'])){
    $UsuarioLogin= $_POST['UsuarioLogin'];
    $PasswordLogin= $_POST['PasswordLogin'];
    $iddatos= $_POST['iddatos'];
    $propietario= $_POST['propietario'];
    $razon= $_POST['razon'];
    $direccion= $_POST['direccion'];
    $nro= $_POST['nro'];
    $telefono= $_POST['telefono'];

    $mensaje = "Se Actualizaron Los Datos De La Empresa";
    $alerta = "alert alert-info";

    $UpdateMensaje = $con->UpdateMensajeAlerta($mensaje, $alerta);

    $UpdateDataFactura = $con->UpdateDataFactura($propietario,$razon,$direccion,$nro,$telefono);
}

header("Location: DatosFactura.php?usuario=$UsuarioLogin&password=$PasswordLogin&estado='Activo'");

?>